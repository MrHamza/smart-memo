package com.hhs.smartmemo.interfaces;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

/**
 * Created by user on 11/02/2016.
 */
public interface OnDateTimeListener extends TimePickerDialog.OnTimeSetListener,DatePickerDialog.OnDateSetListener {
    @Override
    void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth);

    @Override
    void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second);
}
