package com.hhs.smartmemo.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.amulyakhare.textdrawable.TextDrawable;
import com.balysv.materialripple.MaterialRippleLayout;
import com.hhs.smartmemo.R;
import com.hhs.smartmemo.interfaces.DisplayedInMemoRecyclerView;
import com.hhs.smartmemo.models.MemoEvent;
import com.hhs.smartmemo.models.RecyclerViewHeader;
import com.hhs.smartmemo.viewholders.MemoViewHolder;

import java.util.ArrayList;

/**
 * Created by hhsain on 30/01/2016.
 */
public class MemoRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private static final int HEADER=1;
    private static final int MEMO = 2 ;

    private Context context;
    private ArrayList<DisplayedInMemoRecyclerView> memos ;

    public MemoRecyclerViewAdapter(Context _c, ArrayList<DisplayedInMemoRecyclerView> listMemos)
    {
        context = _c ;
        memos = listMemos;
    }

    @Override
    public int getItemViewType(int position) {
        if(memos.get(position) instanceof RecyclerViewHeader)
            return HEADER;

        return MEMO;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType){
            case HEADER :
                //TODO : initialiser le header
                break;
            case MEMO :
                //View v1 = inflater.inflate(R.layout.view_holder_memo_item, parent, false);

                View v1 = MaterialRippleLayout.on(inflater.inflate(R.layout.view_holder_memo_item, parent, false))
                        .rippleOverlay(true)
                        .rippleAlpha(0.2f)
                        .rippleColor(0xFF585858)
                        .rippleHover(true)
                        .create();
                viewHolder = new MemoViewHolder(v1, context);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case HEADER :
                //TODO : remplir le header
                break ;
            case MEMO :
                initMemo((MemoViewHolder)holder,position);
                break;
        }
    }

    private void initMemo(MemoViewHolder vh, int position)
    {
        MemoEvent memoEvent = (MemoEvent)memos.get(position);
        vh.getMemoTitle().setText(memoEvent.getMemoTitle());
        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Thin.ttf");
        vh.getMemoTitle().setTypeface(font);
        if(memoEvent.getMemoTitle().length() > 0)
        {
            //TODO : Get the color from object
            TextDrawable drawable = TextDrawable.builder()
                    .beginConfig()
                    .withBorder(1)
                    .endConfig()
                    .buildRect(("" + memoEvent.getMemoTitle().charAt(0)).toUpperCase(), generateColor());
            vh.getMemoImage().setImageDrawable(drawable);
        }
        //TODO : verifier les autres Icons
    }

    private int generateColor()
    {
        String[] usedColors = {"#62a7c1","#2093cd","#e4c62e","#67bf74","#ff0000"};
        return Color.parseColor(usedColors[randInt(0,4)]);

    }

    private int randInt(int min, int max) {

        return min + (int)(Math.random() * ((max - min) + 1));
    }

    @Override
    public int getItemCount() {
        return memos.size();
    }
}
