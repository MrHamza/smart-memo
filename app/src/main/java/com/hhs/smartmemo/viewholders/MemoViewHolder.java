package com.hhs.smartmemo.viewholders;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hhs.smartmemo.R;
import com.hhs.smartmemo.activities.MemoDetailsActivity;

/**
 * Created by hhsain on 30/01/2016.
 */
public class MemoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    private Context mContext ;
    private TextView memoTitle ;
    private ImageView memoImage;
    private ImageView hasTxt ;
    private ImageView hasDate ;
    private ImageView hasAlarm ;
    private ImageView hasLocation ;
    private ImageView hasRide ;
    private ImageView hasTimer ;


    public MemoViewHolder(View itemView, Context context) {
        super(itemView);
        mContext  = context;
        memoTitle  = (TextView) itemView.findViewById(R.id.memo_title);
        memoImage = (ImageView)itemView.findViewById(R.id.memo_img);
        hasTxt  = (ImageView)itemView.findViewById(R.id.contains_txt);
        hasDate  = (ImageView)itemView.findViewById(R.id.contains_date);
        hasAlarm  = (ImageView)itemView.findViewById(R.id.contains_alarm);
        hasLocation  = (ImageView)itemView.findViewById(R.id.contains_location);
        hasRide  = (ImageView)itemView.findViewById(R.id.contains_ride);
        hasTimer  = (ImageView)itemView.findViewById(R.id.contains_timer);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(mContext, MemoDetailsActivity.class);
        mContext.startActivity(intent);
    }

    public Context getmContext() {
        return mContext;
    }

    public void setmContext(Context mContext) {
        this.mContext = mContext;
    }

    public TextView getMemoTitle() {
        return memoTitle;
    }

    public void setMemoTitle(TextView memoTitle) {
        this.memoTitle = memoTitle;
    }

    public ImageView getMemoImage() {
        return memoImage;
    }

    public void setMemoImage(ImageView memoImage) {
        this.memoImage = memoImage;
    }

    public ImageView getHasTxt() {
        return hasTxt;
    }

    public void setHasTxt(ImageView hasTxt) {
        this.hasTxt = hasTxt;
    }

    public ImageView getHasDate() {
        return hasDate;
    }

    public void setHasDate(ImageView hasDate) {
        this.hasDate = hasDate;
    }

    public ImageView getHasAlarm() {
        return hasAlarm;
    }

    public void setHasAlarm(ImageView hasAlarm) {
        this.hasAlarm = hasAlarm;
    }

    public ImageView getHasLocation() {
        return hasLocation;
    }

    public void setHasLocation(ImageView hasLocation) {
        this.hasLocation = hasLocation;
    }

    public ImageView getHasRide() {
        return hasRide;
    }

    public void setHasRide(ImageView hasRide) {
        this.hasRide = hasRide;
    }

    public ImageView getHasTimer() {
        return hasTimer;
    }

    public void setHasTimer(ImageView hasTimer) {
        this.hasTimer = hasTimer;
    }
}
