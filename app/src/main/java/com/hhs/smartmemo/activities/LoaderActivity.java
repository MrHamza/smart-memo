package com.hhs.smartmemo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.github.jlmd.animatedcircleloadingview.AnimatedCircleLoadingView;
import com.hhs.smartmemo.R;

public class LoaderActivity extends SmartMemoActivity {

    private AnimatedCircleLoadingView loader ;
    private static int DELAY = 5000 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViews();
        loader.startDeterminate();
        loader.stopOk();
        //TODO : FAIRE LES TRAITEMENTS D'INITIALISATION
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent intent = new Intent(LoaderActivity.this,MainActivity.class);
                LoaderActivity.this.startActivity(intent);
            }
        }, DELAY);

    }

    private void initViews() {
        loader = (AnimatedCircleLoadingView)findViewById(R.id.circle_loading_view);
    }

    @Override
    public int getContentView() {
        return R.layout.activity_loader;
    }
}
