package com.hhs.smartmemo.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.hhs.smartmemo.R;
import com.hhs.smartmemo.SmartMemoApplication;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by hhsain on 29/01/2016.
 */
public abstract class AbstractActivity extends AppCompatActivity{
    protected final String TAG = this.getClass().getName();
    protected SmartMemoApplication smApp ;
    protected Toolbar mActionBarToolbar;
    private InputMethodManager mInputMethodManager;
    protected FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentView());
        smApp = (SmartMemoApplication) getApplication();
        mInputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        getActionBarToolbar();

    }

    public Toolbar getActionBarToolbar() {
        if (mActionBarToolbar == null) {
            mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
            if (mActionBarToolbar != null) {
                setSupportActionBar(mActionBarToolbar);
                getSupportActionBar().setHomeButtonEnabled(true);
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowTitleEnabled(false);
                mActionBarToolbar.setNavigationIcon(R.drawable.btn_back);
                mActionBarToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });
            }
        }
        return mActionBarToolbar;
    }

    public void hideKeyboard(View v) {
        if (mInputMethodManager != null && v != null)
            mInputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public abstract int getContentView();
}
