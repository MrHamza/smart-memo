package com.hhs.smartmemo.activities;

import android.app.AlarmManager;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.glomadrian.codeinputlib.CodeInput;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.hhs.smartmemo.R;
import com.hhs.smartmemo.composants.MemoItemCard;
import com.hhs.smartmemo.constants.Constant;
import com.hhs.smartmemo.models.MemoAlarm;
import com.hhs.smartmemo.models.MemoDateInterval;
import com.hhs.smartmemo.models.MemoEvent;
import com.hhs.smartmemo.models.MemoLocation;
import com.hhs.smartmemo.recievers.AlarmReceiver;
import com.jmedeisis.draglinearlayout.DragLinearLayout;
import com.squareup.timessquare.CalendarPickerView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import com.yalantis.contextmenu.lib.ContextMenuDialogFragment;
import com.yalantis.contextmenu.lib.MenuObject;
import com.yalantis.contextmenu.lib.MenuParams;
import com.yalantis.contextmenu.lib.interfaces.OnMenuItemClickListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public  class MemoDetailsActivity extends SmartMemoActivity implements OnMenuItemClickListener,OnMapReadyCallback {

    private ContextMenuDialogFragment mMenuDialogFragment;
    private FragmentManager fragmentManager;
    private DragLinearLayout cardsRootView;
    private View calendarView = null ;
    private View alarmView = null ;
    private View imageView = null ;
    private View mapView = null;
    private View textView = null;
    private View trajetView = null;
    private View intervalView = null ;
    private View secreteView = null ;
    private String localSecret = null ;
    private CodeInput secretInput = null ;
    private SimpleDateFormat dateFormater = new SimpleDateFormat("dd/MM/yyyy - hh:mm");
    private MemoLocation mMemolocation = null ;
    MapFragment mMapFragment = null ;


    private MemoEvent mainMemo ;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentManager = getSupportFragmentManager();
        cardsRootView = (DragLinearLayout)findViewById(R.id.memo_container);
        cardsRootView.setContainerScrollView((ScrollView) findViewById(R.id.scrollable_layout));
        initMemoFromExtras();
        initMenuFragment();
        initViews();
    }

    private void initMemoFromExtras()
    {
        mainMemo = new MemoEvent();
        Calendar calendarTime = Calendar.getInstance();
        mainMemo.setMemoId(calendarTime.getTimeInMillis());
        mainMemo.setMemoTitle("My memo title");
        mainMemo.setMemoContent(getString(R.string.dummy_content_normal));

        calendarTime = Calendar.getInstance();
        MemoAlarm alrm = new MemoAlarm();
        alrm.setAlarmId(calendarTime.getTimeInMillis());
        alrm.setAlarmDate(calendarTime.getTimeInMillis());
        alrm.setRepeat(false);

        mainMemo.setMemoAlarm(alrm);
        calendarTime.add(Calendar.SECOND,5);


    }


    private void initViews(){
        //TODO : INITIALISER LES CARTES SELON LES DONNEES
        Calendar calendarTime = Calendar.getInstance();
        initText(getString(R.string.dummy_content_short),getString(R.string.dummy_content_normal),false);
        //initTrajet(getString(R.string.dummy_content_short),getString(R.string.dummy_content_short));
        //initCalendar(calendarTime.getTimeInMillis());
        //initAlarm(calendarTime.getTimeInMillis());
        //initInterval(calendarTime.getTimeInMillis(),calendarTime.getTimeInMillis());
        //initImage(null);
        //initMap(0,0);
        //initSecret("My code");
    }

    //#####################################
    //######## INIT ELEMENTS   ############
    //#####################################

    private void initSecret(String md5Secret)
    {

        if(secreteView == null )
            secreteView = LayoutInflater.from(this).inflate(R.layout.widget_memo_secret,cardsRootView );
        if(mainMemo != null)
        {
            if(secretInput == null )
                secretInput =  (CodeInput)secreteView.findViewById(R.id.secrete_code);
            localSecret = md5Secret ;
        }

        //TODO implementer au niveau du model

    }

    private void initInterval(long timestamp1 , long timestamp2)
    {
        if(mainMemo != null )
        {
            if(intervalView == null )
                intervalView = LayoutInflater.from(this).inflate(R.layout.widget_memo_interval,cardsRootView );

            Calendar calendar = Calendar.getInstance();
            if(mainMemo.getMemoDateInterval() != null)
            {
                mainMemo.getMemoDateInterval().setDateDebut(timestamp1);
                mainMemo.getMemoDateInterval().setDateFin(timestamp2);
            }
            else {
                MemoDateInterval interval = new MemoDateInterval();
                interval.setDateDebut(timestamp1);
                interval.setDateFin(timestamp2);
                mainMemo.setMemoDateInterval(interval);
            }

            calendar.setTimeInMillis(timestamp1);

            TextView intDepTxtView = (TextView) intervalView.findViewById(R.id.interval_dep);
            intDepTxtView.setText(dateFormater.format(calendar.getTime()));

            calendar.setTimeInMillis(timestamp2);

            TextView intFinTxtView = (TextView) intervalView.findViewById(R.id.interval_fin);
            intFinTxtView.setText(dateFormater.format(calendar.getTime()));
        }

    }

    private void initTrajet(String adrDep , String adrArr)
    {
        if(trajetView == null )
            trajetView = LayoutInflater.from(this).inflate(R.layout.widget_memo_trajet,cardsRootView );
        TextView trajetDep = (TextView) trajetView.findViewById(R.id.trajet_dep);
        TextView trajetArr = (TextView) trajetView.findViewById(R.id.trajet_arr);

        trajetDep.setText(adrDep);
        trajetArr.setText(adrArr);
    }

    private void initImage(Image image)
    {
        if(imageView == null)
            imageView = LayoutInflater.from(this).inflate(R.layout.widget_memo_image,cardsRootView);
        //TODO : Recuperer l'image depuis l'objet et la modifier
    }

    private void initMap(float lat , float lng, boolean editMode)
    {
        if(mainMemo != null )
        {
            if(mapView == null) {
                mapView = View.inflate(this, R.layout.widget_memo_map, null);
                cardsRootView.addDragView(mapView,mapView);
            }
            MemoLocation location = mainMemo.getMemoLocation() ;
            if(editMode)
            {
                if(location != null && location.getLatitude() != 0 &&  location.getLongitute() != 0 )
                {
                    mMemolocation = location ;
                    if(mMapFragment == null) {
                        mMapFragment = MapFragment.newInstance();
                        FragmentTransaction fragmentTransaction =
                                getFragmentManager().beginTransaction();
                        mMapFragment.getMapAsync(this);
                        fragmentTransaction.add(R.id.map_container, mMapFragment);
                        fragmentTransaction.commit();
                    }
                }
                else
                {
                    Toast.makeText(MemoDetailsActivity.this, getString(R.string.note_exists), Toast.LENGTH_SHORT).show();
                }
            }
            else
            {

            }



        }


        //TODO : Recuperer les lat lng depuis l'objet depuis l'objet et la modifier
    }

    private void initCalendar(long timestamp)
    {
        if(mainMemo != null )
        {
            if(calendarView == null )
            {
                calendarView = View.inflate(this, R.layout.widget_memo_date, null);
                cardsRootView.addDragView(calendarView,calendarView);
            }

            final MemoItemCard card = (MemoItemCard)calendarView;

            card.setOnEditClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    performAddDate(false);
                }
            });
            card.setOnDeleteClickListener(new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    removeCalendar();
                }
            });

            mainMemo.setMemoDate(timestamp);
            Calendar calendarTime = Calendar.getInstance();
            calendarTime.setTimeInMillis(timestamp);
            Date mindate = calendarTime.getTime();
            calendarTime.add(Calendar.MONTH,1);
            CalendarPickerView calendar = (CalendarPickerView) calendarView.findViewById(R.id.calendar_view);
            calendar.init(mindate, calendarTime.getTime())
                    .withSelectedDate(mindate)
                    .displayOnly();
        }
    }

    private void initAlarm(long timestamp,boolean isCreateMode)
    {
        if(mainMemo != null )
        {

            if(alarmView != null && isCreateMode)
            {
                Toast.makeText(MemoDetailsActivity.this, getString(R.string.note_exists), Toast.LENGTH_SHORT).show();
                return ;
            }
            else if(alarmView == null && !isCreateMode)
            {
                return ;
            }else if(alarmView == null)
            {
                alarmView = View.inflate(this, R.layout.widget_memo_alarme, null);
                cardsRootView.addDragView(alarmView,alarmView);
            }


            final MemoItemCard card = (MemoItemCard)alarmView;

            card.setOnEditClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    performAddAlarm(false);
                }
            });
            card.setOnDeleteClickListener(new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    removeAlarm();
                }
            });
            Calendar calendarTime = Calendar.getInstance();

            TextView alarmTxt = (TextView)alarmView.findViewById(R.id.alarm_viewer);
            calendarTime.setTimeInMillis(timestamp);
            alarmTxt.setText(dateFormater.format(calendarTime.getTime()));

            if(mainMemo.getMemoAlarm() == null)
            {
                MemoAlarm memoAlarm = new MemoAlarm();
                memoAlarm.setAlarmId(calendarTime.getTimeInMillis());
                memoAlarm.setAlarmDate(timestamp);
                mainMemo.setMemoAlarm(memoAlarm);
            }
            else
            {
                mainMemo.getMemoAlarm().setAlarmDate(timestamp);
            }
            setAlarm(calendarTime);
        }

    }

    private void initText(String title, String content, boolean editMode)
    {

        if(mainMemo != null)
        {

            if(textView == null)
            {
                textView = View.inflate(this, R.layout.widget_memo_txt, null);
                cardsRootView.addDragView(textView,textView);
                Button saveBtn = (Button)textView.findViewById(R.id.enter_edit);
                final EditText newContent = (EditText) textView.findViewById(R.id.memo_edit_details_content);
                final MemoItemCard card = (MemoItemCard)textView;
                saveBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        initText(card.getTitleEditable(), newContent.getText().toString(),false);
                    }
                });

                card.setOnDeleteClickListener(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        removeText();
                    }
                });

                card.setOnEditClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        initText(mainMemo.getMemoTitle(),mainMemo.getMemoContent(),true);
                    }
                });
            }
            EditText newContent = (EditText) textView.findViewById(R.id.memo_edit_details_content);
            MemoItemCard card = (MemoItemCard)textView;

            if(editMode) {
                textView.findViewById(R.id.edition_form).setVisibility(View.VISIBLE);
                textView.findViewById(R.id.memo_details_content).setVisibility(View.GONE);
                card.setTitleEditable(title);
                newContent.setText(content);
            }
            else {
                textView.findViewById(R.id.edition_form).setVisibility(View.GONE);
                textView.findViewById(R.id.memo_details_content).setVisibility(View.VISIBLE);
                mainMemo.setMemoTitle(title);
                mainMemo.setMemoContent(content);
                TextView contentTxt = (TextView)textView.findViewById(R.id.memo_details_content);
                card.disableEditable();
                card.setTitle(title);
                contentTxt.setText(content);
            }
        }
    }

    //#####################################
    //######## REMOVE ELEMENTS ############
    //#####################################

    private void removeText()
    {
        if(textView != null)
        {
            ((ViewManager)textView.getParent()).removeView(textView);
            textView = null;
            if(mainMemo != null) {
                mainMemo.setMemoTitle(null);
                mainMemo.setMemoContent(null);
                Toast.makeText(MemoDetailsActivity.this, getString(R.string.succes_delete), Toast.LENGTH_SHORT).show();
            }

        }
    }

    private void removeAlarm()
    {
        if(alarmView != null)
        {
            Calendar c = Calendar.getInstance();
            c.add(Calendar.YEAR,-1);
            setAlarm(c);
            ((ViewManager)alarmView.getParent()).removeView(alarmView);
            if(mainMemo != null)
                mainMemo.setMemoAlarm(null);
            alarmView = null ;
            Toast.makeText(MemoDetailsActivity.this, getString(R.string.succes_delete), Toast.LENGTH_SHORT).show();
        }
    }

    private void removeCalendar(){
        if(calendarView != null)
        {
            ((ViewManager)calendarView.getParent()).removeView(calendarView);
            if(mainMemo != null)
                mainMemo.setMemoDate(0);
            calendarView = null ;
            Toast.makeText(MemoDetailsActivity.this, getString(R.string.succes_delete), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public int getContentView() {
        return R.layout.activity_memo_details;
    }

    //#####################################
    //######## INIT MENU       ############
    //#####################################

    ArrayList<Integer> ordredMenu ;
    private void initMenuFragment() {
        MenuParams menuParams = new MenuParams();
        menuParams.setActionBarSize((int) getResources().getDimension(R.dimen.tool_bar_height));
        menuParams.setMenuObjects(getMenuObjects());
        menuParams.setClosableOutside(false);
        mMenuDialogFragment = ContextMenuDialogFragment.newInstance(menuParams);
        mMenuDialogFragment.setItemClickListener(this);

    }

    private List<MenuObject> getMenuObjects() {
        List<MenuObject> menuObjects = new ArrayList<>();
        ordredMenu = new ArrayList<>();
        MenuObject close = new MenuObject();
        close.setResource(R.drawable.icn_close);
        menuObjects.add(close);
        ordredMenu.add(Constant.MenuCodes.CLOSE);

        MenuObject send = new MenuObject(getString(R.string.add_txt));
        send.setResource(R.drawable.icn_1);
        menuObjects.add(send);
        ordredMenu.add(Constant.MenuCodes.ADD_NOTE);

        MenuObject like = new MenuObject(getString(R.string.add_date));
        like.setResource(R.drawable.icn_calendar);
        menuObjects.add(like);
        ordredMenu.add(Constant.MenuCodes.ADD_DATE);

        MenuObject addFr = new MenuObject(getString(R.string.add_alarm));
        addFr.setResource(R.drawable.icn_alarm);
        menuObjects.add(addFr);
        ordredMenu.add(Constant.MenuCodes.ADD_ALARM);

        MenuObject addFav = new MenuObject(getString(R.string.add_image));
        addFav.setResource(R.drawable.icn_img);
        menuObjects.add(addFav);
        ordredMenu.add(Constant.MenuCodes.ADD_IMG);

        MenuObject location = new MenuObject(getString(R.string.add_location));
        location.setResource(R.drawable.icn_location);
        menuObjects.add(location);
        ordredMenu.add(Constant.MenuCodes.ADD_LOCATION);

        MenuObject interval = new MenuObject(getString(R.string.add_interval));
        interval.setResource(R.drawable.icn_interval);
        menuObjects.add(interval);
        ordredMenu.add(Constant.MenuCodes.ADD_INTERVAL);

        MenuObject trajet = new MenuObject(getString(R.string.add_trajet));
        trajet.setResource(R.drawable.icn_walk);
        menuObjects.add(trajet);
        ordredMenu.add(Constant.MenuCodes.ADD_TRAJET);

        MenuObject locker = new MenuObject(getString(R.string.add_secret));
        locker.setResource(R.drawable.icn_lock);
        menuObjects.add(locker);
        ordredMenu.add(Constant.MenuCodes.ADD_SECRET);

        return menuObjects;
    }

    @Override
    public void onMenuItemClick(View clickedView, int position) {
        //new SweetAlertDialog(this,SweetAlertDialog.SUCCESS_TYPE)
        //        .setTitleText("Hey !")
        //        .setContentText("Item selected : " + position)
        //        .show();
        if(position <= ordredMenu.size())
            performMenuAction(ordredMenu.get(position));
    }

    //#####################################
    //######## PERFORM ACTIONS ############
    //#####################################

    private void performMenuAction(int actionKey)
    {
        switch (actionKey)
        {
            case Constant.MenuCodes.ADD_ALARM :
                performAddAlarm(true);
                break;
            case Constant.MenuCodes.ADD_DATE :
                performAddDate(true);
                break;
            case Constant.MenuCodes.ADD_IMG :
                break;
            case Constant.MenuCodes.ADD_INTERVAL :
                break;
            case Constant.MenuCodes.ADD_LOCATION:
                break;
            case Constant.MenuCodes.ADD_NOTE:
                performAddNote();
                break;
            case Constant.MenuCodes.ADD_SECRET:
                break;
            case Constant.MenuCodes.ADD_TRAJET:
                break;
            case Constant.MenuCodes.CLOSE:
                break;
        }
    }


    private void performAddDate(final boolean isCreateMode){
        if(calendarView != null && isCreateMode) {
            Toast.makeText(MemoDetailsActivity.this, getString(R.string.note_exists), Toast.LENGTH_SHORT).show();
            return ;
        }
        else if(calendarView == null && !isCreateMode)
        {
            return ;
        }
        Calendar now = Calendar.getInstance();
        final DatePickerDialog dpd = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar selectedDate = Calendar.getInstance();
                        selectedDate.set(Calendar.YEAR,year);
                        selectedDate.set(Calendar.MONTH,monthOfYear);
                        selectedDate.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                        initCalendar(selectedDate.getTimeInMillis());
                    }
                },
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH));

        dpd.show(getFragmentManager(), "Datepickerdialog");



    }

    private Calendar selectedAlarm = null ;
    private void performAddAlarm(final boolean isCreateMode)
    {
        if(alarmView != null && isCreateMode)
        {
            Toast.makeText(MemoDetailsActivity.this, getString(R.string.note_exists), Toast.LENGTH_SHORT).show();
            return ;
        }
        else if(alarmView == null && !isCreateMode)
        {
            return ;
        }

        Calendar now = Calendar.getInstance();
        final TimePickerDialog tpd = TimePickerDialog.newInstance(
        new TimePickerDialog.OnTimeSetListener() {
                   @Override
                   public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
                       if(selectedAlarm != null)
                       {
                           selectedAlarm.set(Calendar.HOUR_OF_DAY, hourOfDay);
                           selectedAlarm.set(Calendar.MINUTE, minute);
                           selectedAlarm.set(Calendar.SECOND, second);
                           initAlarm(selectedAlarm.getTimeInMillis(),isCreateMode);
                       }
                       selectedAlarm = null;
                   }
               },
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE), false);

        final DatePickerDialog dpd = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        selectedAlarm = Calendar.getInstance();
                        selectedAlarm.set(Calendar.YEAR,year);
                        selectedAlarm.set(Calendar.MONTH,monthOfYear);
                        selectedAlarm.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                        tpd.show(getFragmentManager(), "Timepickerdialog");
                    }
                },
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH));

        dpd.show(getFragmentManager(), "Datepickerdialog");

    }

    private void performAddNote(){
        if(textView == null) {
            initText("","",true);
        }
        else
            Toast.makeText(MemoDetailsActivity.this, getString(R.string.note_exists), Toast.LENGTH_SHORT).show();
    }

    //TODO : PerformAddINTERVAL
    private void performAddInterval(boolean isCreateMode) {

    }

    //#####################################
    //######## PRIVATE BUSINISS ############
    //#####################################

    public final static String NOTIFICATION_TITLE = "alarm.title";
    public final static String NOTIFICATION_TEXT = "alarm.corp";
    public final static String MEMO_ID = "memo.id";
    private void setAlarm(Calendar targetCal) {
        if(mainMemo != null && mainMemo.getMemoAlarm() != null )
        {
            Intent intent = new Intent(getBaseContext(), AlarmReceiver.class);
            intent.putExtra(NOTIFICATION_TITLE, getString(R.string.app_name));
            intent.putExtra(NOTIFICATION_TEXT, mainMemo.getMemoTitle());
            intent.putExtra(MEMO_ID, mainMemo.getMemoId());
            PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(), (int)mainMemo.getMemoId(), intent, PendingIntent.FLAG_ONE_SHOT);
            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            alarmManager.set(AlarmManager.RTC_WAKEUP, targetCal.getTimeInMillis(), pendingIntent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.context_menu:
                if (fragmentManager.findFragmentByTag(ContextMenuDialogFragment.TAG) == null) {
                    mMenuDialogFragment.show(fragmentManager, ContextMenuDialogFragment.TAG);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    // _____________ On map Ready _______________
    @Override
    public void onMapReady(GoogleMap googleMap) {

    }
}
