package com.hhs.smartmemo.activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;

import com.hhs.smartmemo.R;
import com.hhs.smartmemo.adapters.MemoRecyclerViewAdapter;
import com.hhs.smartmemo.interfaces.DisplayedInMemoRecyclerView;
import com.hhs.smartmemo.models.MemoEvent;

import java.util.ArrayList;
import java.util.Random;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class MainActivity extends SmartMemoActivity {

    private RecyclerView memoRecyclerView ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.app_name);
        initRecyclerView();
        displayDayQuote();

    }

    public void initRecyclerView() {
        memoRecyclerView = (RecyclerView)findViewById(R.id.memo_recycler_view);
        // Create adapter passing in the sample user data
        MemoRecyclerViewAdapter adapter = new MemoRecyclerViewAdapter( this, generateDummyContent(20));
        // Attach the adapter to the recyclerview to populate items
        memoRecyclerView.setAdapter(adapter);
        // Set layout manager to position the items
        memoRecyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    private ArrayList<DisplayedInMemoRecyclerView> generateDummyContent(int nbr)
    {
        ArrayList<DisplayedInMemoRecyclerView> result = new ArrayList<>();
        for(int i = 0 ; i<nbr ; i++)
        {
            MemoEvent e = new MemoEvent();
            e.setMemoTitle("Memo title " + i);
            result.add(e);
        }

        return result;
    }

    private void displayDayQuote()
    {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
            //TODO : afficher la  citation une fois par jour
            new SweetAlertDialog(MainActivity.this,SweetAlertDialog.NORMAL_TYPE)
                    .setTitleText(getString(R.string.day_quote))
                    .setContentText(getRandomQuote())
                    .show();
            }
        }, 2000);

    }

    private String getRandomQuote()
    {
        String[] tab = getResources().getStringArray(R.array.quotes_array);
        Random rand = new Random();
        try{
            return tab[rand.nextInt(tab.length)];
        }
        catch(Exception e)
        {
            return "#ff0000";
        }

    }



    @Override
    public int getContentView() {
        return R.layout.activity_main;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

}
