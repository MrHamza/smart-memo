package com.hhs.smartmemo.constants;

/**
 * Created by hhsain on 02/02/2016.
 */
public class Constant {
    public static class MenuCodes{
        public final static int CLOSE = 0 ;
        public final static int ADD_NOTE = 1 ;
        public final static int ADD_DATE = 2 ;
        public final static int ADD_ALARM = 3 ;
        public final static int ADD_IMG = 4 ;
        public final static int ADD_LOCATION = 5 ;
        public final static int ADD_INTERVAL = 6 ;
        public final static int ADD_TRAJET = 7 ;
        public final static int ADD_SECRET = 8 ;
    }

}
