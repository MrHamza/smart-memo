package com.hhs.smartmemo.models;

import com.hhs.smartmemo.interfaces.DisplayedInMemoRecyclerView;

/**
 * Created by user on 30/01/2016.
 */
public class RecyclerViewHeader implements DisplayedInMemoRecyclerView{
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
