package com.hhs.smartmemo.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by hhsain on 29/01/2016.
 */
public class MemoImage extends RealmObject{

    @PrimaryKey
    private long imageId;
    private String imageLocation ;
    private String imageInformation ;

    public long getImageId() {
        return imageId;
    }

    public void setImageId(long imageId) {
        this.imageId = imageId;
    }

    public String getImageLocation() {
        return imageLocation;
    }

    public void setImageLocation(String imageLocation) {
        this.imageLocation = imageLocation;
    }

    public String getImageInformation() {
        return imageInformation;
    }

    public void setImageInformation(String imageInformation) {
        this.imageInformation = imageInformation;
    }
}
