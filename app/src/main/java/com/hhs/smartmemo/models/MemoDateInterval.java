package com.hhs.smartmemo.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by hhsain on 29/01/2016.
 */
public class MemoDateInterval extends RealmObject {
    @PrimaryKey
    private long dateIntervalId ;
    private long dateDebut;
    private long dateFin ;

    public long getDateIntervalId() {
        return dateIntervalId;
    }

    public void setDateIntervalId(long dateIntervalId) {
        this.dateIntervalId = dateIntervalId;
    }

    public long getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(long dateDebut) {
        this.dateDebut = dateDebut;
    }

    public long getDateFin() {
        return dateFin;
    }

    public void setDateFin(long dateFin) {
        this.dateFin = dateFin;
    }
}
