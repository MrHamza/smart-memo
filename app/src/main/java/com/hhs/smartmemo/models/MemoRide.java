package com.hhs.smartmemo.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by hhsain on 29/01/2016.
 */
public class MemoRide extends RealmObject{

    @PrimaryKey
    private long rideId;

    private float latBegin ;
    private float lngBegin ;

    private float latEnd ;
    private float lngEnd ;

    private String adrDep;
    private String adrEnd;

    public String getAdrDep() {
        return adrDep;
    }

    public void setAdrDep(String adrDep) {
        this.adrDep = adrDep;
    }

    public String getAdrEnd() {
        return adrEnd;
    }

    public void setAdrEnd(String adrEnd) {
        this.adrEnd = adrEnd;
    }

    public long getRideId() {
        return rideId;
    }

    public void setRideId(long rideId) {
        this.rideId = rideId;
    }

    public float getLatBegin() {
        return latBegin;
    }

    public void setLatBegin(float latBegin) {
        this.latBegin = latBegin;
    }

    public float getLngBegin() {
        return lngBegin;
    }

    public void setLngBegin(float lngBegin) {
        this.lngBegin = lngBegin;
    }

    public float getLatEnd() {
        return latEnd;
    }

    public void setLatEnd(float latEnd) {
        this.latEnd = latEnd;
    }

    public float getLngEnd() {
        return lngEnd;
    }

    public void setLngEnd(float lngEnd) {
        this.lngEnd = lngEnd;
    }
}
