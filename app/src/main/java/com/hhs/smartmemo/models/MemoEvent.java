package com.hhs.smartmemo.models;

import com.hhs.smartmemo.interfaces.DisplayedInMemoRecyclerView;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by hhsain on 29/01/2016.
 */
public class MemoEvent extends RealmObject implements DisplayedInMemoRecyclerView {

    @PrimaryKey
    private long memoId ;
    private String memoTitle ;
    private String memoContent ;
    private long memoDate;
    private MemoImage memoImage;
    private MemoLocation memoLocation;
    private MemoRide memoRide;
    private MemoDateInterval  memoDateInterval;
    private MemoAlarm memoAlarm;

    public long getMemoId() {
        return memoId;
    }

    public void setMemoId(long memoId) {
        this.memoId = memoId;
    }

    public String getMemoTitle() {
        return memoTitle;
    }

    public void setMemoTitle(String memoTitle) {
        this.memoTitle = memoTitle;
    }

    public String getMemoContent() {
        return memoContent;
    }

    public void setMemoContent(String memoContent) {
        this.memoContent = memoContent;
    }

    public long getMemoDate() {
        return memoDate;
    }

    public void setMemoDate(long memoDate) {
        this.memoDate = memoDate;
    }

    public MemoImage getMemoImage() {
        return memoImage;
    }

    public void setMemoImage(MemoImage memoImage) {
        this.memoImage = memoImage;
    }

    public MemoLocation getMemoLocation() {
        return memoLocation;
    }

    public void setMemoLocation(MemoLocation memoLocation) {
        this.memoLocation = memoLocation;
    }

    public MemoRide getMemoRide() {
        return memoRide;
    }

    public void setMemoRide(MemoRide memoRide) {
        this.memoRide = memoRide;
    }

    public MemoDateInterval getMemoDateInterval() {
        return memoDateInterval;
    }

    public void setMemoDateInterval(MemoDateInterval memoDateInterval) {
        this.memoDateInterval = memoDateInterval;
    }

    public MemoAlarm getMemoAlarm() {
        return memoAlarm;
    }

    public void setMemoAlarm(MemoAlarm memoAlarm) {
        this.memoAlarm = memoAlarm;
    }
}
