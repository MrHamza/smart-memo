package com.hhs.smartmemo;

import android.app.Application;
import android.content.SharedPreferences;

import com.hhs.smartmemo.helpers.DataManager;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by hhsain on 29/01/2016.
 */
public class SmartMemoApplication extends Application{

    public static final String PREF_KEY = "prefs.smartmemo.hhs.com";
    private SharedPreferences mSharedPrefs;
    DataManager mDataManager ;

    @Override
    public void onCreate() {
        super.onCreate();

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Roboto-Thin.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        mSharedPrefs = getSharedPreferences(PREF_KEY, MODE_PRIVATE);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this).build();
        Realm.setDefaultConfiguration(realmConfiguration);
        mDataManager = new DataManager(getApplicationContext());
    }

    public DataManager getDataManager() {
        return mDataManager;
    }

    /**
     * Fermer la base de données
     */
    @Override
    public void onTerminate() {
        mDataManager.terminate();
        super.onTerminate();
    }
}
