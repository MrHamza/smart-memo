package com.hhs.smartmemo.helpers;

import android.content.Context;

import com.hhs.smartmemo.models.MemoEvent;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by hhsain on 29/01/2016.
 */
public class DataManager {
    private Realm realm;

    public DataManager(Context context) {
        realm = Realm.getInstance(context);
    }

    /**
     * Close de database
     */
    public void terminate() {
        if (realm != null) {
            realm.close();
        }
    }

    /**
     * Inserer une nouvelle memo
     * @param memo
     */
    public void insertNewMemo(MemoEvent memo){
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(memo);
        realm.commitTransaction();
    }

    /**
     * Recuperer la liste de tous les memos de l'utilisateur
     * @return
     */
    public ArrayList<MemoEvent> getAllMemos() {
        ArrayList<MemoEvent> resultList = new ArrayList<>();
        RealmQuery<MemoEvent> query = realm.where(MemoEvent.class);
        RealmResults<MemoEvent> realmResult = query.findAll() ;
        for(MemoEvent ev : realmResult)
            resultList.add(ev);

        return resultList ;
    }

}
