package com.hhs.smartmemo.composants;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.hhs.smartmemo.R;

/**
 * Created by hhsain on 31/01/2016.
 */
public class MemoItemCard extends LinearLayout{
    private TextView titleTxt ;
    private ImageView titleIcn;
    private int icnSrc;
    private String titleStr ;
    private LinearLayout mContentView;
    private EditText titleEditor;
    private ImageButton deleteBtn ;
    private ImageButton editBtn ;
    private View memoHeader ;
    private SwipeLayout swipeLayout ;


    public MemoItemCard(final Context context) {
        this(context, null);
    }

    public MemoItemCard(final Context context,
                        final AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MemoItemCard(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);

        final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.MemoItemCard, defStyle, 0);
        icnSrc =  a.getResourceId(R.styleable.MemoItemCard_memo_title_icn, 0);
        titleStr = a.getString(R.styleable.MemoItemCard_memo_title_txt);
        a.recycle();

        init(context);
    }
    private void init(Context context) {
        View rootView = LayoutInflater.from(context).inflate(R.layout.component_memo_title, this, true);
        mContentView = (LinearLayout) rootView.findViewById(R.id.memo_component_container);
        titleTxt = (TextView) rootView.findViewById(R.id.com_mt_txt);
        titleIcn = (ImageView) rootView.findViewById(R.id.com_mt_icn);
        titleEditor = (EditText) rootView.findViewById(R.id.et_mt);
        deleteBtn = (ImageButton) rootView.findViewById(R.id.delete_btn_action);
        editBtn = (ImageButton) rootView.findViewById(R.id.edit_btn_action);
        memoHeader = rootView.findViewById(R.id.memo_component_header);
        swipeLayout = (SwipeLayout)rootView.findViewById(R.id.swip_interface);

        memoHeader.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mContentView.getVisibility() == GONE) {
                    swipeLayout.setLeftSwipeEnabled(true);
                    swipeLayout.setRightSwipeEnabled(true);
                    mContentView.setVisibility(VISIBLE);
                }
                else {
                    swipeLayout.setLeftSwipeEnabled(false);
                    swipeLayout.setRightSwipeEnabled(false);
                    mContentView.setVisibility(GONE);
                }
            }
        });

        if(titleStr != null)
            titleTxt.setText(titleStr);
        if(icnSrc != 0)
            setIcon(icnSrc);

    }

    public void setOnDeleteClickListener(final DialogInterface.OnClickListener onclick)
    {
        deleteBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(MemoItemCard.this.getContext())
                        .setCancelable(true)
                        .setMessage(MemoItemCard.this.getContext().getString(R.string.alert_delete_item))
                        .setNegativeButton(MemoItemCard.this.getContext().getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setPositiveButton(MemoItemCard.this.getContext().getString(R.string.dialog_yes), onclick);

                final AlertDialog dialog = alertBuilder.create();

                dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface arg0) {
                        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                    }
                });

                dialog.show();
            }
        });
    }

    public void setOnEditClickListener(OnClickListener onclick)
    {
        editBtn.setOnClickListener(onclick);
    }
    @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params) {
        if(mContentView == null){
            super.addView(child, index, params);
        } else {
            mContentView.addView(child, index, params);
        }
    }


    public void setTitleEditable(String title) {
        titleEditor.setText(title);
        titleEditor.setVisibility(VISIBLE);
        titleTxt.setVisibility(GONE);
    }

    public String getTitleEditable()
    {
        return titleEditor.getText().toString();
    }

    public void disableEditable() {
        titleEditor.setVisibility(GONE);
        titleTxt.setVisibility(VISIBLE);
    }

    public void setTitle(String str)
    {
        titleTxt.setText(str);
    }

    public void setIcon(int res)
    {
        Log.i("MemoItemCard","ID RESOURCE F: " + res);
        titleIcn.setVisibility(VISIBLE);
        titleIcn.setImageResource(res);
    }

    public TextView getTitleView()
    {
        return titleTxt;
    }

}
