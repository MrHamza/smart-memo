package com.hhs.smartmemo.recievers;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;

import com.hhs.smartmemo.R;
import com.hhs.smartmemo.activities.MemoDetailsActivity;

/**
 * Created by hhsain on 02/02/2016.
 */
public class AlarmReceiver extends BroadcastReceiver {

    static final int NOTIFICATION_ID = 2541;
    @Override
    public void onReceive(Context k1, Intent k2) {
        Bundle bundle = k2.getExtras();
        String notificationContent = null, notificationTitle = null;
        long memoId = 0 ;
        if(bundle != null)
        {
            notificationContent = bundle.getString(MemoDetailsActivity.NOTIFICATION_TEXT);
            notificationTitle = bundle.getString(MemoDetailsActivity.NOTIFICATION_TITLE);
            memoId = bundle.getLong(MemoDetailsActivity.MEMO_ID);
        }
        if(notificationContent != null  && notificationTitle != null)
        {
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(k1);
            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            mBuilder.setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(notificationTitle)
                    .setContentText(notificationContent)
                    .setSound(alarmSound);
            NotificationManager mNotifyMgr = (NotificationManager) k1.getSystemService(k1.NOTIFICATION_SERVICE);
            mNotifyMgr.notify((int)memoId, mBuilder.build());
        }

    }

}